import React from 'react';
import { SectionList, View, Text, StyleSheet } from 'react-native';
import SQLite from 'react-native-sqlite-storage';

export interface Transaction {
    PK: Number,
    tranDate: Date,
    majorCategoryName: String,
    subCategoryName: String,
    amount: Number,
}

export interface TransactionsListSectionData {
    title: string
    data: Array<TransactionsListCellDisplayData>,
}

// data displayed in a table cell
export interface TransactionsListCellDisplayData {
    PK?: Number,
    tranDate?: Date,
    dayOrdinal: String,
    majorCategoryName?: String,
    subCategoryName?: String,
    amount?: Number
}

class Transactions extends React.Component {
    sectionsDataArr = new Array<TransactionsListSectionData>();

    constructor(props: any) {
        super(props)

        this.state = {
            transactionsSections: Array<TransactionsListSectionData>()
        }
    }

    componentDidMount() {
        console.log("componentDidMount()");
        var this2 = this;
        this.getTransactions(function (trans: Array<Transaction>, errorMsg?: string) {
            this2.formatForUI(trans);
            this2.setState(this2.sectionsDataArr); // TODO
        });
    }

    getTransactions(callback: (result: Array<Transaction>, errorMessage?: string) => void): void {
        console.log("getTransactions()");
        let dbParams: SQLite.DatabaseParams = {
            name: "TheCash",
            createFromLocation: "~data/TheCash.sqlite",
            location: "Documents"
        }

        var db: SQLite.SQLiteDatabase = SQLite.openDatabase(
            dbParams,
            () => {
                console.log("Success: opened database");
            },
            (e: SQLite.SQLError) => {
                console.error("ERROR: Opening database: " + e.code + "; " + e.message);
                callback(null, e.message);
            });

        if (db) {
            db.transaction(
                (tx) => { // transaction as in database transaction, not a cash transaction
                    let sqlStmt: string = `select a.Z_PK, strftime('%Y-%m-%d', datetime(ZTRANDATE + 978307200, 'unixepoch', 'localtime')) as tranDate, b.ZMAJORCATEGORYNAME, c.ZSUBCATEGORYNAME, ZTRANAMOUNT 
                    from ZTRANSACTIONDATAENTITY a, ZMAJORCATEGORYDATAENTITY b, ZSUBCATEGORYDATAENTITY c 
                    where a.ZMAJORCATEGORY = b.Z_PK and a.ZSUBCATEGORY = c.Z_PK
                    order by tranDate desc`;

                    tx.executeSql(
                        sqlStmt,
                        [],
                        (tx: SQLite.Transaction, results: SQLite.ResultSet) => {
                            console.log("getTransactions(): SQL query completed");
                            var newRow: Transaction;
                            var transactions = new Array<Transaction>();
                            var len = results.rows.length;
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                let tranDate = new Date(row.tranDate);
                                newRow = { PK: row.Z_PK, tranDate: tranDate, majorCategoryName: row.ZMAJORCATEGORYNAME, subCategoryName: row.ZSUBCATEGORYNAME, amount: row.ZTRANAMOUNT };
                                transactions.push(newRow);
                            }
                            callback(transactions, null);
                        },
                        (tx: SQLite.Transaction, e: SQLite.SQLError) => {
                            console.error("ERROR: getting transactions: " + e.code + "; " + e.message);
                            callback(null, e.message);
                        }
                    );
                }
            );
        }
    }

    //
    // Splice in some section headers
    //
    formatForUI(transactions: Array<Transaction>) {
        console.log("formatForUI()");
        var prevYr = 1800;
        var prevMth = 1;
        var prevDay = 1;
        let mthArr = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        var cellDisplayData: TransactionsListCellDisplayData;
        var cellDisplayDataForSectionArr = new Array<TransactionsListCellDisplayData>();

        var sectionHeader: string;
        var sectionData: TransactionsListSectionData;

        for (let transaction of transactions) {
            let yr = transaction.tranDate.getFullYear();
            let mth = transaction.tranDate.getMonth();

            if ((yr != prevYr) || (mth != prevMth)) { // section header
                // write data for the previous section
                if (cellDisplayDataForSectionArr.length > 0) {
                    sectionData = {
                        title: sectionHeader,
                        data: cellDisplayDataForSectionArr
                    }
                    this.sectionsDataArr.push(sectionData);
                }

                // set up the new section header, ready for the next section
                prevYr = yr;
                prevMth = mth;
                var month = transaction.tranDate.getMonth();
                let monthStr = mthArr[month];
                sectionHeader = String(yr) + " " + monthStr;

                // reset the cells array, ready for the next section
                cellDisplayDataForSectionArr = new Array<TransactionsListCellDisplayData>();
            }

            // add data for a single cell
            cellDisplayData = {
                PK: transaction.PK,
                tranDate: transaction.tranDate,
                dayOrdinal: this.dayOrdinal(transaction.tranDate),
                majorCategoryName: transaction.majorCategoryName,
                subCategoryName: transaction.subCategoryName,
                amount: transaction.amount
            };
            cellDisplayDataForSectionArr.push(cellDisplayData);
        }

        // write the final section
        if (cellDisplayDataForSectionArr.length > 0) {
            sectionData = {
                title: sectionHeader,
                data: cellDisplayDataForSectionArr
            }
            this.sectionsDataArr.push(sectionData);
        }
    }

    dayOrdinal(displayDate: Date): String {
        let day = displayDate.getUTCDate();
        var dispWithOrd: String;

        switch (day) {
            case 1:
            case 21:
            case 31:
                dispWithOrd = day + "st";
                break;
            case 2:
            case 22:
                dispWithOrd = day + "nd";
                break;
            case 3:
            case 23:
                dispWithOrd = day + "rd";
                break;
            default:
                dispWithOrd = day + "th";
                break;
        }

        return dispWithOrd;
    }

    render() {
        console.log("render()");
        return (
            <SectionList
                sections={this.sectionsDataArr}
                renderItem={
                    ({ item }) =>
                        <View style={this.styles.cellHorizontalContainer}>
                            <Text style={this.styles.dayComponent}>{item.dayOrdinal}</Text>

                            <View style={this.styles.cellVerticalContainer}>
                                <Text style={this.styles.majorCategoryComponent}>{item.majorCategoryName}</Text>
                                <Text style={this.styles.subCategoryComponent}>{item.subCategoryName}</Text>
                            </View>

                            <Text style={this.styles.amountComponent}>{new Intl.NumberFormat('en-GB', {
                                style: 'currency',
                                currency: 'AUD'
                            }).format(item.amount)}</Text>
                        </View>
                }
                renderSectionHeader={({ section }) => <Text style={this.styles.sectionHeader}>{section.title}</Text>}
                keyExtractor={ (item, index ) => item.PK.toString() }
            />
        );
    }

    styles = StyleSheet.create({
        cellVerticalContainer: {
            flex: 1,
            flexDirection: 'column'
        },
        cellHorizontalContainer: {
            flex: 1,
            flexDirection: 'row'
        },
        sectionHeader: {
            paddingTop: 2,
            paddingLeft: 10,
            paddingRight: 10,
            paddingBottom: 2,
            fontSize: 14,
            fontWeight: 'bold',
            backgroundColor: 'rgba(247,247,247,1.0)',
        },
        dayComponent: { 
            fontSize: 18,
            paddingLeft: 10,
            alignSelf: "center"
        },
        majorCategoryComponent: {
            fontSize: 18,
            paddingTop: 5,
            paddingLeft: 10
        },
        subCategoryComponent: {
            fontSize: 16,
            paddingLeft: 10,
            paddingBottom: 5
        },
        amountComponent: {
            fontSize: 16,
            paddingLeft: 10,
            paddingRight: 10,
            alignSelf: "center"
        }     
    });
}

export default Transactions
