import React from 'react';
import { NavigatorIOS } from 'react-native';
import Transactions from './transactions';
class App extends React.Component {
    render() {
        let myRoute = {
            component: Transactions,
            title: "Milestone Computing Pty Ltd"
        };
        return (<NavigatorIOS initialRoute={myRoute} style={{ flex: 1 }}>
            </NavigatorIOS>);
    }
}
export default App;
//# sourceMappingURL=App.js.map